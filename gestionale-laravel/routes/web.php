<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WebsiteController@index');

// === resource crea tutte le routes e i relativi comandi HTTP
// === per gestire il CRUD
Route::resource('/anagrafica', 'AnagraficaController');

Route::get('/demo-ws-ajax', 'AnagraficaController@demo');

Route::get('/ws-ricerca-nominativo/{nome?}','WsAnagraficaController@ricercaNominativo');