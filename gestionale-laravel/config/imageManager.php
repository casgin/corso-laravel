<?php

return [
  'sizes'   => [
      'thumbnail' => [
          'width'   => 300,
          'height'  => 300,
          'mode'    => 'crop',
          'quality' => 90
      ],

      'icon' => [
          'width'   => 100,
          'height'  => 100,
          'mode'    => 'crop',
          'quality' => 90
      ],

  ]
];