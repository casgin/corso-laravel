<?php

namespace App\Http\Controllers;

use App\Anagrafica;
use Illuminate\Http\Request;

class AnagraficaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // === Istanzio il model anagrafica
        $dati = new \App\Anagrafica();

        \Debugbar::addMessage('Benvenuti nel nostro applicativo', 'Welcome');
        \Debugbar::info($dati);


        return view('anagrafica.index')
                ->with([
                    //'anagrafica' => $dati::all()->toArray()     // --- Recupero TUTTI i record di Anagrafica
                    'anagrafica' => $dati::paginate(5)     // --- Recupero PAGINATI i record di Anagrafica
                ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('anagrafica.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // ==== Validazione dei dati  ====
        // make:request VerificaAnagraficaRequest
        // store(VerificaAnagraficaRequest $request)


        $anagrafica = new \App\Models\Anagrafica();

        $anagrafica->nome = $request->get('nome');
        $anagrafica->cognome = $request->get('cognome');
        $anagrafica->email = $request->get('email');
        $anagrafica->tipo = $request->get('tipo');

        // --- caso di default
        $anagrafica->foto = '';

        // === Meccanismo di gestione e caricamento file
        if(
           $request->hasFile('foto') && $request->get('foto')!=''
            && $request->file('foto')->isValid()
        )
        {
            // --- Recupero l'estensione del file caricato
            $extension = $request->foto->extension();
            $nomeFile = str_slug(
                        strtolower($request->get('nome').'-'.$request->get('cognome'))
                    ).'.'.$extension;

            $path = $request->foto->storeAs(
                'images',           // nome della cartella interna al disco
                $nomeFile,            // nome del file da caricare
                'uploads'           // nome del "disco" definito in config/filesystems.php
            );

            // === Popolo il campo della tabella
            $anagrafica->foto = $path;

        }

        $anagrafica->save();

        return redirect('/anagrafica');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Anagrafica  $anagrafica
     * @return \Illuminate\Http\Response
     */
    public function show(Anagrafica $anagrafica)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Anagrafica  $anagrafica
     * @return \Illuminate\Http\Response
     */
    public function edit(Anagrafica $anagrafica)
    {
        \Debugbar::disable();

        return view('anagrafica.edit')
                ->with(['anagrafica' => $anagrafica]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Anagrafica  $anagrafica
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Anagrafica $anagrafica)
    {
        $anagrafica->nome = $request->get('nome');
        $anagrafica->cognome = $request->get('cognome');
        $anagrafica->email = $request->get('email');
        $anagrafica->tipo = $request->get('tipo');
        $anagrafica->foto = '';

        $anagrafica->save();

        return view('anagrafica.index')->with([
            'msg' => 'Record Modificato ID='.$anagrafica->id
        ]);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Anagrafica  $anagrafica
     * @return \Illuminate\Http\Response
     */
    public function destroy(Anagrafica $anagrafica)
    {
        //
    }


    public function demo()
    {
        return view('anagrafica.demo');
    }
}
