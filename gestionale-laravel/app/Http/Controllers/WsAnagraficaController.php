<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WsAnagraficaController extends Controller
{
    public function ricercaNominativo($nome=null, Request $request)
    {
        /*
        $arDatiEsempio = [
                [
                    'nome'  => 'Gianfranco',
                    'cognome'  => 'Castro',
                    'eta'  => 38,
                ]
        ];
        */
        # http://127.0.0.1:8000/ws-ricerca-nominativo?callback=jQueryVIVE
        # http://127.0.0.1:8000/ws-ricerca-nominativo
        # http://127.0.0.1:8000/ws-ricerca-nominativo/gigi?callback=?

        $condition = '%';
        if(!is_null($nome)) {
            $condition=$nome.'%';
        }

        $anagrafica = new \App\Models\Anagrafica();
        $dati = $anagrafica::where('nome', 'like', $condition)
                            ->orderBy('nome')
                            ->paginate(15)
                            ->toArray();

        return response()
                    ->json($dati)
                    ->withCallback($request->input('callback'));

    }
}
