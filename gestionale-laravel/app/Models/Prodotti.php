<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 01 Apr 2017 08:44:57 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Prodotti
 * 
 * @property int $id
 * @property string $codice_prodotto
 * @property string $nome
 * @property string $descrizione
 * @property float $prezzo
 * @property string $disponibile
 * 
 * @property \Illuminate\Database\Eloquent\Collection $dettaglio_ordinis
 *
 * @package App\Models
 */
class Prodotti extends Eloquent
{
	protected $table = 'prodotti';
	public $timestamps = false;

	protected $casts = [
		'prezzo' => 'float'
	];

	protected $fillable = [
		'codice_prodotto',
		'nome',
		'descrizione',
		'prezzo',
		'disponibile'
	];

	public function dettaglio_ordinis()
	{
		return $this->hasMany(\App\Models\DettaglioOrdini::class);
	}
}
