<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 01 Apr 2017 08:44:57 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DettaglioOrdini
 * 
 * @property int $id
 * @property int $ordini_id
 * @property int $quantita
 * @property int $prodotti_id
 * 
 * @property \App\Models\Ordini $ordini
 * @property \App\Models\Prodotti $prodotti
 *
 * @package App\Models
 */
class DettaglioOrdini extends Eloquent
{
	protected $table = 'dettaglio_ordini';
	public $timestamps = false;

	protected $casts = [
		'ordini_id' => 'int',
		'quantita' => 'int',
		'prodotti_id' => 'int'
	];

	protected $fillable = [
		'ordini_id',
		'quantita',
		'prodotti_id'
	];

	public function ordini()
	{
		return $this->belongsTo(\App\Models\Ordini::class);
	}

	public function prodotti()
	{
		return $this->belongsTo(\App\Models\Prodotti::class);
	}
}
