<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 01 Apr 2017 08:44:56 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Anagrafica
 * 
 * @property int $id
 * @property string $nome
 * @property string $cognome
 * @property string $email
 * @property int $tipo
 * @property string $foto
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $ordinis
 *
 * @package App\Models
 */
class Anagrafica extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'anagrafica';

	protected $casts = [
		'tipo' => 'int'
	];

	protected $fillable = [
		'nome',
		'cognome',
		'email',
		'tipo',
		'foto'
	];

	public function ordinis()
	{
		return $this->hasMany(\App\Models\Ordini::class);
	}
}
