<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 01 Apr 2017 08:44:57 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Ordini
 * 
 * @property int $id
 * @property int $nunero_ordine
 * @property \Carbon\Carbon $data_ordine
 * @property float $totale_ordine
 * @property string $stato_ordine
 * @property int $anagrafica_id
 * 
 * @property \App\Models\Anagrafica $anagrafica
 * @property \Illuminate\Database\Eloquent\Collection $dettaglio_ordinis
 *
 * @package App\Models
 */
class Ordini extends Eloquent
{
	protected $table = 'ordini';
	public $timestamps = false;

	protected $casts = [
		'nunero_ordine' => 'int',
		'totale_ordine' => 'float',
		'anagrafica_id' => 'int'
	];

	protected $dates = [
		'data_ordine'
	];

	protected $fillable = [
		'nunero_ordine',
		'data_ordine',
		'totale_ordine',
		'stato_ordine',
		'anagrafica_id'
	];

	public function anagrafica()
	{
		return $this->belongsTo(\App\Models\Anagrafica::class);
	}

	public function dettaglio_ordinis()
	{
		return $this->hasMany(\App\Models\DettaglioOrdini::class);
	}
}
