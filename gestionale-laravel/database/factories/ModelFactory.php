<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
/*
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});
*/

$factory->define(App\Anagrafica::class, function(Faker\Generator $faker){

    return [
        'nome' => $faker->firstName,
        'cognome' => $faker->lastName,
        'email' => $faker->email,
        'tipo' => $faker->numberBetween(1,3),
        'foto' => $faker->image(public_path()
            .DIRECTORY_SEPARATOR.'foto-caricate'
            .DIRECTORY_SEPARATOR.'images'

        )
    ];

    /**
     * Ricordarsi di eseguire a file seed
     * la query
     * update anagrafica set foto=replace(foto, '/Users/gcastro/laravelapps/CorsoLaravel/gestionale-laravel/public/foto-caricate/', '');
     */

});