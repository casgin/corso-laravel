<?php

use Illuminate\Database\Seeder;

class PopulateAnagrafica extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
        // Inserisco le informazioni per creare un record dove mi serve
        $mdlAnagrafica = new App\Anagrafica();

        $mdlAnagrafica->nome='Seed Nome';
        $mdlAnagrafica->cognome='Seed Cognome';
        $mdlAnagrafica->email='test.test@test.it';
        $mdlAnagrafica->foto = '';
        $mdlAnagrafica->tipo = 1;

        $mdlAnagrafica->save();
        */

        factory(App\Anagrafica::class, 800)->create();
    }
}
