@extends('layout.main-layout')

@section('mainContent')
    <h1>Interrogazione del servizio</h1>

    <div id="result">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>id</th>
                    <th>nome</th>
                    <th>cognome</th>
                </tr>
            </thead>
            <tbody id="tableRow"></tbody>
        </table>
    </div>

@endsection


@section('scriptZone')
    <script>
        $(document).ready(function(){

            $.getJSON(
                '/ws-ricerca-nominativo?callback=?',
                function(result) {
                    console.log(result.data);

                    $(result.data).each(function(idx,item){

                        var tr = $('<tr>');
                        tr.attr('id', item.id);
                        tr.attr('class', 'rigo');

                        var out='';
                        out+='<td>'+item.id+'</td>';
                        out+='<td>'+item.nome+'</td>';
                        out+='<td>'+item.cognome+'</td>';

                        tr.html(out);

                        $('#tableRow').append(tr);

                    });

                    // === A tabella Renderizzata sul DOM, associo un eventListener al rigo
                    // === della tabella
                    $('.rigo').on('click',function () {
                        alert($(this).attr('id'));
                        $(this).fadeOut();
                    })


                }
            );


        });
    </script>

@endsection
