@extends('layout.main-layout')

@section('mainContent')
    <div class="row">
        <h1>Gestione Anagrafica</h1>

        {{--
            Devo specificare:
            - metodo HTTP (default POST)
            - che viene effettuato upload del file
            - route name di destinazione
        --}}
        {!! Form::open(
                [
                    'route' => 'anagrafica.store',
                    'method' => 'POST',
                    'id' => 'frmInserisciNuovo',
                    'files' => true
                ]) !!}

            {{-- Elenco dei campi della maschera  --}}
            @include('includes.anagrafica-form')

        {!! Form::close() !!}

    </div>
@endsection