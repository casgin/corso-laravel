@extends('layout.main-layout')

@section('mainContent')
    <div class="row">
        <h1>Gestione Anagrafica</h1>
        <a href="{!! route('anagrafica.create') !!}" class="btn btn-primary">Inserisci Nuovo</a>
    </div>

    {{-- Visualizzo messaggio di notifica --}}
    @if(isset($msg))
        <div class="row">
            <p class="bg-success">{{$msg}}</p>
        </div>
    @endif
<!--
<?php
print_r($anagrafica);
?>
-->
    {{-- Tabella HTML, Bootstrap compliant--}}
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>Cognome</th>
                <th>Foto</th>
                <th>Action</th>
            </tr>
        </thead>


        <tbody>
        @foreach($anagrafica as $k => $item)
            <tr>
                <td>{{$item['id']}}</td>
                <td>{{$item['nome']}}</td>
                <td>{{$item['cognome']}}</td>
                <td>
                    @if(isset($item['foto']) && $item['foto']!=='')
                        <img src="/foto-caricate/{{$item['foto']}}"
                             alt="Foto di {{$item['nome']}} {{$item['cognome']}}"
                             width="100"
                                />
                    @else
                        <strong>Foto non disponibile</strong>
                    @endif
                </td>

                <!-- Action disponibili -->
                <td>
                    <!-- Mostra dettaglio record -->
                    {{link_to_route(
                        'anagrafica.show',              // nome del named-route
                        'Mostra',                       // testo del link
                        ['id' => $item['id']],          // array associativo con i parametri da passare
                        ['title' => 'Mostra dettaglio'] // attributi HTML custom
                        )}}
                    &nbsp;|&nbsp;

                    <!-- Modifica -->
                    {{link_to_route(
                        'anagrafica.edit',
                        'Modificat',
                        ['id' => $item['id']],
                        ['title' => 'Modifica Record']
                        )}}

                    &nbsp;|&nbsp;
                    <!-- Cancellazione -->
                    {{link_to_route(
                        'anagrafica.destroy',
                        'Elimina',
                        ['id' => $item['id']],
                        ['title' => 'Mostra dettaglio']
                        )}}

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{-- Genera una struttura UL/LI/A Bootstrap compliant --}}
    {{$anagrafica->links()}}

    {{-- Visualizzazione tabella con PAGINAZIONE --}}
@endsection