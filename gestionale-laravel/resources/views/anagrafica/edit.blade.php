@extends('layout.main-layout')

@section('mainContent')
    <div class="row">
        <h1>Gestione Anagrafica</h1>
        <h2>Modifica Record</h2>

        {{--
            Devo specificare:
            - metodo HTTP (default POST)
            - che viene effettuato upload del file
            - route name di destinazione
        --}}
        {!! Form::model(
                $anagrafica,
                [
                    'route' => ['anagrafica.update', $anagrafica->id],
                    'method' => 'PUT',
                    'id' => 'frmModificaAnagrafica',
                    'files' => true
                ]) !!}

        {{-- Elenco dei campi della maschera  --}}
        @include('includes.anagrafica-form')

        {!! Form::close() !!}

    </div>
@endsection