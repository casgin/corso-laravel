<div class="form-group">
    {!! Form::label('nome','Nome') !!}
    {!! Form::text('nome',null, ['class' => 'form-control', 'placeholder' => 'Inserisci il tuo nome']) !!}
</div>

<div class="form-group">
    {!! Form::label('cognome','Cognome') !!}
    {!! Form::text('cognome',null, ['class' => 'form-control', 'placeholder' => 'Inserisci il tuo cognome']) !!}
</div>

<div class="form-group">
    {!! Form::label('email','Email') !!}
    {!! Form::email('email',null, ['class' => 'form-control', 'placeholder' => 'Inserisci indirizzo email']) !!}
</div>

<div class="form-group">
    {!! Form::label('tipo','Tipo Contatto') !!}
    {!! Form::select(
            'tipo',
            [1 => 'Cliente', 2 => 'Fornitore', 3 => 'Consulente'],
            null,
            ['placeholder' => 'Seleziona una tipologia di contatto']) !!}
</div>

{{-- Upload del file --}}
<div class="form-group">
    {!! Form::label('foto', 'Avatar') !!}
    {!! Form::file('foto') !!}
</div>



<div class="form-group">
    {!! Form::submit('Invia Dati', ['class' => 'btn btn-primary']) !!}
</div>

















