@extends('layout.main-layout')

@section('mainContent')
<div class="starter-template">
    <h1>Gestionale Laravel</h1>
    <p class="lead">
        Esercitazione su come costruire un C.R.U.D.
        con Laravel
    </p>
</div>
@endsection